const admin = require("firebase-admin");
const functions = require('firebase-functions');
const fs = require('fs');
const serviceAccount = require("./credentials/dbCredentials.json");

// let collectionName = process.argv[2];
let collectionName = 'routes';
let subCollection = process.argv[3];

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://ibid-32cbe.firebaseio.com"
});

let db = admin.firestore();
db.settings({ timestampsInSnapshots: true });

let data = {};
data[collectionName] = {};

let results = db.collection(collectionName)
.get()
.then(snapshot => {
  snapshot.forEach(doc => {
    data[collectionName][doc.id] = doc.data();
  })
  return data;
})
.catch(error => {
  console.log(error);
})

results.then(dt => {  
  getSubCollection(dt).then(() => {   
    fs.writeFile("./src/assets/i18n/" + collectionName + ".json", JSON.stringify(data), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
  })  
})

async function getSubCollection(dt){
  for (let [key, value] of Object.entries([dt[collectionName]][0])){
    if(subCollection !== undefined){
      data[collectionName][key]['subCollection'] = {};
      await addSubCollection(key, data[collectionName][key]['subCollection']);            
    }          
  }  
}

function addSubCollection(key, subData){
  return new Promise(resolve => {
    db.collection(collectionName).doc(key).collection(subCollection).get()
    .then(snapshot => {      
      snapshot.forEach(subDoc => {             
        subData[subDoc.id] =  subDoc.data();
        resolve('Added data');                                                                 
      })
    })
  })
}
