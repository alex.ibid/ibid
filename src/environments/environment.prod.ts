export const environment = {
  production: true,
  appName: 'Ibid. Web Development. Alexandre Santos.',
  envName: 'PROD',
  test: false,
  firebase: {
    apiKey: "AIzaSyDLQxG5N-qudSwtjIf61DalvKGqXapiHpI",
    authDomain: "ibid-32cbe.firebaseapp.com",
    databaseURL: "https://ibid-32cbe.firebaseio.com",
    projectId: "ibid-32cbe",
    storageBucket: "ibid-32cbe.appspot.com",
    messagingSenderId: "42192423778"
  }
};
