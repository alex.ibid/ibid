// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  appName: 'Ibid. Web Development. Alexandre Santos.',
  envName: 'DEV',
  test: false,
  firebase: {
    apiKey: "AIzaSyDLQxG5N-qudSwtjIf61DalvKGqXapiHpI",
    authDomain: "ibid-32cbe.firebaseapp.com",
    databaseURL: "https://ibid-32cbe.firebaseio.com",
    projectId: "ibid-32cbe",
    storageBucket: "ibid-32cbe.appspot.com",
    messagingSenderId: "42192423778"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
