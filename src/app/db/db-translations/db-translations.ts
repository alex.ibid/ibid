import { TranslateLoader } from '@ngx-translate/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs/internal/Observable';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';

export class DbTranslations implements TranslateLoader {

  constructor(
    private db: AngularFirestore
  ) { }

  public getTranslation(language: string): Observable<any> {
    const translationsDoc = this.db.doc(`translations/${language}`).valueChanges();
    const routesDoc = this.db.doc(`routes/${language}`).valueChanges();
    const translations = combineLatest<any[]>(translationsDoc, routesDoc).pipe(map(each => {
      return each.reduce((translations,routes) => {
        let merged = {...translations, ...routes};
        return merged;
      })
    }));
    return translations;
  }

}
