import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class DbService {

  dbCollection: AngularFirestoreCollection<any>;
  dbDocument:   AngularFirestoreDocument<any>;

  constructor(private db: AngularFirestore, private http: HttpClient) {
    
  }

  getRouter(){
    return this.http.get("assets/router.json");
}

  getTranslation(collection: string, lang: string): Observable<any> {
    return this.db.doc(`${collection}/${lang}`).snapshotChanges();
  }

  getCol(collection: string) {
    return this.db.collection(collection).snapshotChanges();
  }

  getCollLang(coll, lang){
    return this.db.collection(coll).doc(lang).snapshotChanges();
  }

  getData(coll, lang, subcoll){
    return this.db.collection(coll).doc(lang).collection(subcoll).snapshotChanges();
  }

  getCollection(collection): Observable<any[]> {
    this.dbCollection = this.db.collection(collection);
    return this.dbCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((a) => {
          const data = a.payload.doc.data();
          return { id: a.payload.doc.id, ...data };
        });
      })
    );
  }

  getDocument(collection, id: string) {
    return this.db.doc<any>(`${collection}/${id}`);
  }

  // helper to remove when backoffice or not
  addDocument(slug: string, language: string, payload: Object) {
    this.db.collection(slug).doc(language).set(payload)
  }

  addData(collection, section, language, type, payload){
    this.db.collection(collection).doc(section).collection(language).doc(type).set(payload)
  }

  getRoutes() {
    return this.getCol('translations').pipe(
      map(actions => {
        const routes = actions.map((a, i) => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          const prop = Object.keys(data).filter(each => each.indexOf('ROUTES') !== -1 );
          let object = [];
          let route = prop.map(split => {
            const obj = {};
            obj[split.split('.')[1]] = data[split];
            // console.log(split.split('.')[1]);
            switch (split.split('.')[1]) {
              case 'home':
                return { path: data[split], loadChildren: './pages/home/home.module#HomeModule' };
              case 'cv':
                return { path: data[split], loadChildren: './pages/cv/cv.module#CvModule' };
              case 'skills':
                return { path: data[split], loadChildren: './pages/skills/skills.module#SkillsModule' };
              case 'projects':
                return { path: data[split], loadChildren: './pages/projects/projects.module#ProjectsModule' };
              case 'about':
                return { path: data[split], loadChildren: './pages/about/about.module#AboutModule' };
              case 'ui':
                return { path: data[split], loadChildren: './pages/ui/ui.module#UiModule' };
              default:
                break;
            }
          });
          const routes = route.filter(i => i);
          return routes;
        });
        return routes;
      })
    );
  }
  
}
