import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DbService } from './db.service';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CoreModule,

  ],
  declarations: [],
  providers: [DbService]
})
export class DbModule { }
