interface Language {
    code: string;
    name: string;
}

export var LANGUAGES: Language[] = [
    { code: 'en', name: 'English' },
    { code: 'pt', name: 'Português' },
    { code: 'de', name: 'Deutsche' },
    { code: 'fr', name: 'Français' },
    { code: 'es', name: 'Espagnol' },
];

export type LanguageType = 'en' | 'de' | 'fr' | 'es' | 'pt';