import { BrowserModule } from '@angular/platform-browser';
import { NgModule, PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { AppRoutingModule } from './routes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { environment } from '../environments/environment';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { AppComponent } from './app.component';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { SettingsModule } from './settings';

import { LayoutModule } from './layout/layout.module';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { DbTranslations } from './db/db-translations/db-translations';

import { isPlatformBrowser } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { DbService } from './db/db.service';


export function DbTranslationsFactory(
  db: AngularFirestore
) {
  return new DbTranslations(db);
}


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ibid' }),
    BrowserAnimationsModule,

    HttpClientModule,
    
    SettingsModule,

    TranslateModule.forRoot({
      loader: {provide: TranslateLoader, useFactory: DbTranslationsFactory, deps: [AngularFirestore]}
    }),
    
    CoreModule,
    SharedModule,
    AppRoutingModule,
    
    LayoutModule,

    AngularFireModule.initializeApp(environment.firebase, 'ibid'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [ DbService ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}