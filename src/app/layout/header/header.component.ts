import { Component, Input} from '@angular/core';
import { ibidNav } from '../../routes';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  navigation = ibidNav;
  @Input() title;
  @Input() logo;

}
