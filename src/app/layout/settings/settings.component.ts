import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { LANGUAGES } from '../../languages';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../core';
import { ActionSettingsChangeLanguage, selectSettingsLanguage, selectSettingsTheme, ActionSettingsChangeTheme } from '../../settings';
import { Observable } from 'rxjs/internal/Observable';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

import * as RoutingJson from "../../../assets/i18n/routes.json";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  sidenavOpen = true;
  
  themeOpen = false;
  theme$: Observable<string>;

  languageOpen = false;
  languages = LANGUAGES.map(({code}) => code);
  language$: Observable<string>;

  @Output() sidenavState = new EventEmitter<boolean>();

  events: string[] = [];
  opened: boolean;

  constructor(
    public router: Router,
    private store: Store<AppState>,
    public translate: TranslateService
  ){ }

  ngOnInit(){
    this.theme$ = this.store.pipe(select(selectSettingsTheme));
    this.language$ = this.store.pipe(select(selectSettingsLanguage));
  }

  sidenavToggle(){
    this.sidenavOpen = !this.sidenavOpen;
    this.sidenavState.emit(this.sidenavOpen);
  }

  languageToggle(){
    this.languageOpen = this.languageOpen ? false : true;
  }

  languageSelection($event) {
    const language = $event.value;
    const currentSlug = this.router.url.substring(1);
    let routeKey; 
    LANGUAGES.forEach(lang => {
      Object.keys(RoutingJson.routes[lang.code]).forEach(key => {
        if(RoutingJson.routes[lang.code][key] === currentSlug) {
          routeKey = key;
        }
      })
    });
    this.store.dispatch(new ActionSettingsChangeLanguage({ language }));
    this.router.navigateByUrl(RoutingJson.routes[language][routeKey]);

  }

  themeToggle() {
    this.themeOpen = this.themeOpen ? false : true;
  }

  themeSelection($event) {
    const theme = $event.value;
    this.store.dispatch(new ActionSettingsChangeTheme({ theme }));
  }

}

