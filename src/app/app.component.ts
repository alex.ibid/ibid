import { Component, ChangeDetectorRef, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { MediaMatcher } from '@angular/cdk/layout';
import { TranslateService } from '@ngx-translate/core';
import { Store, select } from '@ngrx/store';
import { AppState } from './core/core.state';
import { selectSettingsTheme, selectSettingsLanguage } from './settings/settings.selectors';
import { Observable } from 'rxjs/internal/Observable';
import { SeoService } from './core/seo/seo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingsComponent } from './layout/settings/settings.component';
import { LANGUAGES } from './languages';
import { DbService } from './db/db.service';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {

  loaded: boolean = false;

  breakpoints: MediaQueryList;
  private _breakpointListener: () => void;

  logo = require('../assets/images/logo-ibid-dot.svg');

  sidenavState: boolean;
  @ViewChild('settingsComponent') private settingsComponent: SettingsComponent;

  // language$: Observable<string>;
  seo$: Observable<string>;
  theme$: Observable<string>;
  routes$: Observable<any>;

  routes: any[] = [];

  constructor(
    private router: Router,
    public db: DbService,
    public translate: TranslateService,
    public seo: SeoService,
    public route: ActivatedRoute,
    changeDetectorRef: ChangeDetectorRef, 
    breakpoint: MediaMatcher,
    private store: Store<AppState>
  ){
    
    this.breakpoints = breakpoint.matchMedia('(min-width: 993px)');
    this._breakpointListener = () => changeDetectorRef.detectChanges();
    this.breakpoints.addListener(this._breakpointListener);

    this.sidenavState = this.breakpoints.matches ? true : false;
  }

  ngOnInit(){

    this.theme$ = this.store.pipe(select(selectSettingsTheme));
    this.seo.setSeo( this.route.snapshot, this.translate );
    this.seo$ = this.seo.header;

  }

  toggleSidenav($event){

    if($event === 'backdropClick') {
      this.sidenavState = false;
      this.settingsComponent.sidenavToggle();
    }
    if(typeof $event === 'boolean') {
      this.sidenavState = $event;
    }
    
  }

  ngOnDestroy(): void {
    this.breakpoints.removeListener(this._breakpointListener);
  }
}