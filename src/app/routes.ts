import { NgModule }             from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

import { NotFoundComponent } from './pages/not-found/not-found.component';

import * as RoutingJson from "../assets/i18n/routes.json";

import { LANGUAGES } from './languages';

interface Links {
  link: string;
  label: string;
  children?: Links[];
}

export var ibidNav: Links[] = [
    { link: 'cv', label: 'main.menu.cv' },
    { link: 'skills', label: 'main.menu.skills' },
    { link: 'projects', label: 'main.menu.projects' },
    { link: 'about', label: 'main.menu.about' },
    { link: 'ui', label: 'main.menu.ui', 
      children: [
        { link: 'ui', label: 'main.menu.ui'},
        { link: 'state', label: 'main.menu.ui.state'}
      ]
    },
];

const languages = LANGUAGES.map(({code}) => code);


const ibidRoutes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: "home", loadChildren: "./pages/home/home.module#HomeModule"},
    { path: "ui", loadChildren: "./pages/ui/ui.module#UiModule"},
    { path: "skills", loadChildren: "./pages/skills/skills.module#SkillsModule"},
    { path: "projects", loadChildren: "./pages/projects/projects.module#ProjectsModule"},
    { path: "cv", loadChildren: "./pages/cv/cv.module#CvModule"},
    { path: "about", loadChildren: "./pages/about/about.module#AboutModule"},
    { path: '**', component: NotFoundComponent, data: { title: 'main.menu.notfoundpage' } }
];

@NgModule({
    imports: [
      RouterModule.forRoot(ibidRoutes),
    ],
    exports: [ RouterModule ]
  })
export class AppRoutingModule {
  constructor( private router: Router ){
    this.getRoutes( RoutingJson );
  }

  getRoutes( routesJson: any ) {
    // necessario investigar
    routesJson = routesJson.default;
    const JSONroutes = routesJson.routes;
    //
    let routes: Routes = [ 
      { path: '', pathMatch: 'full', redirectTo: 'home-page' } 
    ];
    languages.forEach(lang => {
      this.router.config.forEach(route => {
        if (JSONroutes[lang][route.path] !== undefined) {
          routes.push({path: JSONroutes[lang][route.path], loadChildren: route.loadChildren });
        }
      });
    });
    routes.push({ path: '**', component: NotFoundComponent, data: { title: 'main.menu.notfoundpage' } });
    this.router.resetConfig(routes);
  }
}
