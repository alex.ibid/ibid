import { Component, OnInit } from '@angular/core';
import { SeoService } from '../../core/seo/seo.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(seo: SeoService) { 
    seo.metaTags('about titulo','about muito longa descriçao','https://loremflickr.com/320/240');
  }

  ngOnInit() {
  }

}
