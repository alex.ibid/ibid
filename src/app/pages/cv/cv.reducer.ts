import * as actions from './cv.actions';
import { createFeatureSelector } from '@ngrx/store';

import { EntityState, createEntityAdapter } from '@ngrx/entity';

import { Cv } from './cv.model';

export const cvAdapter = createEntityAdapter<Cv>();
export interface State extends EntityState<Cv> { }

export const initialState: State = cvAdapter.getInitialState();

export function cvReducer(
  state: State = initialState,
  action: actions.CvActions
  ) {
    switch (action.type) {

      case actions.CV_ADDED:
          return cvAdapter.addOne(
              action.payload, 
              state
            )

      case actions.CV_MODIFIED:
          return cvAdapter.updateOne(
              { 
                  id: action.payload.id, 
                  changes: action.payload 
              }, 
              state
            )
    
      case actions.CV_REMOVED:
          return cvAdapter.removeOne(
              action.payload.id, 
              state
            )

      default:
              return state;
  }
}

export const getCvState = createFeatureSelector<State>('cv');

export const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
  } = cvAdapter.getSelectors(getCvState);