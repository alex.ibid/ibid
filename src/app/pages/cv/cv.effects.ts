import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { AngularFirestore } from '@angular/fire/firestore';
import { Cv } from './cv.model';
import { Action } from '@ngrx/store';

import * as cvActions from './cv.actions';
import { Observable } from 'rxjs/internal/Observable';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { map } from 'rxjs/internal/operators/map';
import { from } from 'rxjs/internal/observable/from';
 
@Injectable({
  providedIn: 'root'
})
export class CvEffects {

    constructor(
        public db: AngularFirestore,
        public actions$: Actions
    ) {}

    @Effect()
    getCv$: Observable<Action> = this.actions$.pipe(
        ofType(cvActions.CV_LOAD),
        switchMap(action => this.db.collection<Cv>('cv').stateChanges()),
        switchMap(actions =>  actions),
        map(action => {
            return {
                type: `[CV] ${action.type}`,
                payload: { id: action.payload.doc.id, ...action.payload.doc.data() }
            };
        })
    );


    @Effect() 
    updateCv$: Observable<Action> = this.actions$.pipe(
        ofType(cvActions.CV_UPDATE),
        map((action: cvActions.CvUpdate) => action),
        switchMap(data => {
            const ref = this.db.doc<Cv>(`cv/${data.id}`)
            return from( ref.update(data.changes) )
        }),
        map(() => new cvActions.CvSuccess())
    );
 
}