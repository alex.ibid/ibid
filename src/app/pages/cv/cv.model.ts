export class Cv {
    id: string;
    dates: string;
    description: string;
    logo: string;
    projects: [];
    role: string;
    subtitle: string;
    title: string;
}
