import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CvRoutingModule } from './cv-routing.module';

import { SharedModule } from '../../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { cvReducer } from './cv.reducer';
import { CvEffects } from './cv.effects';
import { environment } from '../../../environments/environment';

import { CvComponent } from './cv.component';
import { CvCardComponent } from './cv-card/cv-card.component';


@NgModule({
  declarations: [
    CvComponent,
    CvCardComponent
  ],
  imports: [
    CommonModule,

    StoreModule.forFeature('cv', cvReducer),
    EffectsModule.forFeature([CvEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, 
      logOnly: environment.production, 
    }),
    
    SharedModule,
    CvRoutingModule
  ],
  entryComponents: [ CvCardComponent, CvComponent ]
})
export class CvModule { }
