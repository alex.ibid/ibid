import { Action } from '@ngrx/store';
import { Cv } from './cv.model';


export const CV_LOAD = '[CV] load';
export const CV_ADDED    = '[CV] added';
export const CV_MODIFIED = '[CV] modified';
export const CV_REMOVED  = '[CV] removed';
export const CV_UPDATE   = '[CV] update';
export const CV_SUCCESS  = '[CV] update success';

export class CvLoad implements Action {
  readonly type = CV_LOAD;
  constructor() {}
}

export class CvAdded implements Action {
  readonly type = CV_ADDED;
  constructor(public payload: Cv) {}
}

export class CvModified implements Action {
  readonly type = CV_MODIFIED;
  constructor(public payload: Cv) {}
}

export class CvRemoved implements Action {
  readonly type = CV_REMOVED;
  constructor(public payload: Cv) {}
}

export class CvUpdate implements Action {
  readonly type = CV_UPDATE;
  constructor(
      public id: string,
      public changes: Partial<Cv>,
    ) { }
}

export class CvSuccess implements Action {
  readonly type = CV_SUCCESS;
  constructor() {}
}

export type CvActions = 
  CvLoad | 
  CvAdded | 
  CvModified | 
  CvRemoved | 
  CvUpdate | 
  CvSuccess; 