import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

import { Cv } from './cv.model';
import { Store } from '@ngrx/store';

import * as actions from './cv.actions';
import * as fromCv from './cv.reducer';
import { SeoService } from '../../core/seo/seo.service';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.scss']
})
export class CvComponent implements OnInit {

  cvs$: Observable<Cv[]>;

  constructor(
    private db: AngularFirestore,
    private store: Store<fromCv.State>,
    seo: SeoService
  ) { 
    this.store.dispatch( new actions.CvLoad() );
    seo.metaTags('cv titulo','cv muito longa descriçao','https://loremflickr.com/320/240');
  }


  ngOnInit() {
    this.cvs$ = this.store.select(fromCv.selectAll);
  }

  /* 
  addcv: Cv = {
    dates: '2006 to 2011',
    role: 'Junior Architect',
    description: 'Hands-on architectural work, such as drafting and 3D modeling, to the business side such as preparing for presentations and maintaining project files. Conduct research on sites, such as zoning laws and rules that apply for out-of-state projects, always supervised by the senior architect.',
    logo: 'https://firebasestorage.googleapis.com/v0/b/ibid-32cbe.appspot.com/o/qb-logo.png?alt=media&token=73f6454b-a2c1-4447-abf8-1dc8f0527886',
    projects: [],
    subtitle: 'Architectural work supervised by the senior Architect.',
    title: 'Q+B arquitectura e design',
  } 
  */

  /* 
  addCv(addCv: Cv) {
    this.db.addDocument('cv', addCv)
  } 
  */

  addLanguage() {
    this.db.collection('translations').doc('pt').set({
      "ROUTES.home": "pagina-inicial",
  "ROUTES.cv": "curriculo",
  "ROUTES.skills": "competencias",
  "ROUTES.projects": "projectos",
  "ROUTES.about": "sobre",
  "ROUTES.notfoundpage": "pagina-nao-encontrada",
  "main.about.change-theme": "Mudar Tema",
  "main.header.gitlab": "Repositório Gitlab",
  "main.menu.home": "Pagina inicial",
  "main.menu.cv": "Currículo",
  "main.menu.skills": "Competências",
  "main.menu.projects": "Projetos",
  "main.menu.about": "Sobre",
  "main.menu.notfoundpage": "página não encontrada",
  "main.menu.login": "Iniciar sessão",
  "main.menu.logout": "Encerrar sessão",
  "main.menu.settings": "Definições",
  "main.settings.animations.elements": "Elementos de navegação deslizam para cima",
  "main.settings.animations.page": "Navegação com transição em toda pagina",
  "main.settings.animations.title": "Animações",
  "main.settings.general.language.de": "Alemão",
  "main.settings.general.language.en": "Inglês",
  "main.settings.general.language.es": "Espanhol",
  "main.settings.general.language.fr": "Francês",
  "main.settings.general.language.pt": "Português",
  "main.settings.general.placeholder": "Lingua",
  "main.settings.general.title": "Geral",
  "main.settings.themes.blue": "Blue",
  "main.settings.themes.dark": "Dark",
  "main.settings.themes.light": "Light",
  "main.settings.themes.nature": "Nature",
  "main.settings.themes.night-mode": "Modo noturno automático (de 21:00 até 7:00)",
  "main.settings.themes.placeholder": "Color Theme",
  "main.settings.themes.saneago": "Saneago",
  "main.settings.themes.sticky-header": "",
  "main.settings.themes.title": "Themes",
  "main.settings.title": "Configurações",
  "main.title.long": "Alexandre Miguel Lourenço Santos",
  "main.title.short": "Alexandre Santos"
    });
  }

}
