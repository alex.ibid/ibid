import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiComponent } from './ui.component';
import { UiRoutingModule } from './ui-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { DbService } from '../../../app/db/db.service';
import { UistateComponent } from './uistate/uistate.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [UiComponent, UistateComponent],
  imports: [
    CommonModule,

    SharedModule,
    UiRoutingModule
  ],
  providers: [
    DbService
  ]
})
export class UiModule { }
