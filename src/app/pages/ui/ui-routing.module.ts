import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UiComponent } from './ui.component';
import { UistateComponent } from './uistate/uistate.component';

const uiRoutes: Routes = [
  { path: '', component: UiComponent },
  { path: 'state', component: UistateComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(uiRoutes),
  ],
  exports: [RouterModule]
})
export class UiRoutingModule { }
