import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UistateComponent } from './uistate.component';

describe('UistateComponent', () => {
  let component: UistateComponent;
  let fixture: ComponentFixture<UistateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UistateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UistateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
