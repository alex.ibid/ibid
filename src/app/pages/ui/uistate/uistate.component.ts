import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../core';

@Component({
  selector: 'app-uistate',
  templateUrl: './uistate.component.html',
  styleUrls: ['./uistate.component.scss']
})
export class UistateComponent implements OnInit {

  constructor(public store: Store<AppState>) { }

  ngOnInit() {
  }

}
