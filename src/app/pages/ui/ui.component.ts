import { Component, OnInit } from '@angular/core';
import { DbService } from '../../../app/db/db.service';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.scss']
})
export class UiComponent implements OnInit {

  constructor(
    private db: DbService
  ) { }

  ngOnInit() {}

  addDoc(){
    const payload = {
      "body": "Este é o texto do corpo da página inicial do site ibid. Ele deve conter um texto muito longo que deve ser editado quando a cópia for revisada.",
      "metadescription": "Esta é a meta descrição da página inicial que deve ser editada para que o comprimento do copy não seja maior que 158 caracteres. Também deve ser usado para o meta social.",
      "pagetitle": "Ibid. homepage, o site do desenvolvedor web e arquiteto Alexandre Santos.",
      "subtitle": "Este é o subtítulo de ibid. homepage deve ser editado quando a cópia estiver pronta.",
      "title": "Bem-vindo ao ibid. site."
    }
    this.db.addDocument('home','pt', payload);
  }

}