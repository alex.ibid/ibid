import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Store, select } from '@ngrx/store';

import * as fromHome from './home.reducer';
import * as actions from './home.actions';

import { SeoService } from '../../core/seo/seo.service';
import { AppState } from '../../core';
import { selectSettingsLanguage } from '../../../app/settings';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  home$: Observable<any>;
  language$: Observable<string>;

  constructor(
    public store: Store<AppState>,
    seo: SeoService
  ) { 
    
    seo.metaTags('homepage titulo','homepage muito longa descriçao','https://loremflickr.com/320/240');

  }

  ngOnInit() {

    this.home$ = this.store.pipe(select(fromHome.selectEntities));
    this.language$ = this.store.pipe(select(selectSettingsLanguage));

    this.store.dispatch( new actions.HomeLoad() );

  }

}
