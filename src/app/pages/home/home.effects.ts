import { Injectable } from "@angular/core";
import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, switchMap, withLatestFrom } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { Home } from './home.model';
import { Observable } from 'rxjs/internal/Observable';
import { from } from 'rxjs/internal/observable/from';
import { Action, select, Store } from '@ngrx/store';

import * as homeActions from './home.actions';

import { State, selectSettingsLanguage, SettingsActionTypes } from '../../settings';
 
@Injectable({
  providedIn: 'root'
})
export class HomeEffects {

    constructor(
        private fs: AngularFirestore,
        private store: Store<State>,
        public actions$: Actions
    ) {}

    @Effect()
    getHome$: Observable<Action> = this.actions$.pipe(
        ofType(homeActions.HOME_LOAD, SettingsActionTypes.CHANGE_LANGUAGE),
        withLatestFrom(this.store.pipe(select(selectSettingsLanguage))),
        switchMap((lang) => {
            const language = lang[1];
            const data = this.fs.collection<Home>('home', changeLanguage => changeLanguage.where('language', '==', language)).stateChanges();
            return data;
        }),
        switchMap(actions => actions),
        map(action => {
            return {
                type: `[HOME] ${action.type}`,
                payload: { id: action.payload.doc.id, ...action.payload.doc.data() }
            };
        })
    );
    
    @Effect() 
    updateHome$: Observable<Action> = this.actions$.pipe(
        ofType(homeActions.HOME_UPDATE),
        map((action: homeActions.HomeUpdate) => action),
        switchMap(data => {
            const ref = this.fs.doc<Home>(`home/${data.id}`)
            return from( ref.update(data.changes) )
        }),
        map(() => new homeActions.HomeSuccess())
    );
 
}