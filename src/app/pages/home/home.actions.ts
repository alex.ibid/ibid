import { Action } from '@ngrx/store';
import { Home } from './home.model';

export const HOME_LOAD              = '[HOME] load';
export const HOME_ADDED             = '[HOME] added';
export const HOME_LANGUAGE_CHANGE   = '[HOME] language change';
export const HOME_MODIFIED          = '[HOME] modified';
export const HOME_REMOVED           = '[HOME] removed';
export const HOME_UPDATE            = '[HOME] update';
export const HOME_SUCCESS           = '[HOME] update success';

export class HomeLoad implements Action {
  readonly type = HOME_LOAD;
  constructor() {}
}

export class HomeAdded implements Action {
  readonly type = HOME_ADDED;
  constructor(public payload: Home) {}
}

export class HomeModified implements Action {
  readonly type = HOME_MODIFIED;
  constructor(public payload: Home) {}
}

export class HomeRemoved implements Action {
  readonly type = HOME_REMOVED;
  constructor(public payload: Home) {}
}

export class HomeUpdate implements Action {
  readonly type = HOME_UPDATE;
  constructor(
      public id: string,
      public changes: Partial<Home>,
    ) { }
}

export class HomeSuccess implements Action {
  readonly type = HOME_SUCCESS;
  constructor() {}
}

export type HomeActions = 
  HomeLoad | 
  HomeAdded | 
  HomeModified | 
  HomeRemoved | 
  HomeUpdate | 
  HomeSuccess; 