export class Home {
    id: string;
    payload: {
        title: string;
    }
}

export class Header {
    title: string;
    subtitle: string;
}
