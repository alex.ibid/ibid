import * as actions from './home.actions';
import { createFeatureSelector } from '@ngrx/store';

import { EntityState, createEntityAdapter } from '@ngrx/entity';

import { Home } from './home.model';

export const homeAdapter = createEntityAdapter<Home>();
export interface HomeState extends EntityState<Home> { }

export const initialState: HomeState = homeAdapter.getInitialState();

export function homeReducer(
  state: HomeState = initialState,
  action: actions.HomeActions
  ) {
    switch (action.type) {

        case actions.HOME_ADDED:
        
            return homeAdapter.addOne(
                action.payload, 
                state
            )

        case actions.HOME_MODIFIED:
            return homeAdapter.updateOne(
                { 
                    id: action.payload.id, 
                    changes: action.payload 
                }, 
                state
            )
        
        case actions.HOME_REMOVED:
            return homeAdapter.removeOne(
                action.payload.id, 
                state
            )

        default:
            return state;
  }
}

export const getHomeState = createFeatureSelector<HomeState>('home');

export const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
  } = homeAdapter.getSelectors(getHomeState);