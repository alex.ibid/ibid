import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SkillsComponent } from './skills.component';

const skillsRoutes: Routes = [
  { path: '', component: SkillsComponent, data: { title: 'main.menu.skills' } }
];

@NgModule({
  imports: [
    RouterModule.forChild(skillsRoutes),
  ],
  exports: [RouterModule]
})
export class SkillsRoutingModule { }
