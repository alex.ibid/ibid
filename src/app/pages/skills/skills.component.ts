import { Component, OnInit } from '@angular/core';
import { SeoService } from '../../core/seo/seo.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  constructor(seo: SeoService) { 
    seo.metaTags('skills titulo','skills muito longa descriçao','https://loremflickr.com/320/240');
  }

  ngOnInit() {
  }

}
