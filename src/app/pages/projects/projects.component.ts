import { Component, OnInit } from '@angular/core';
import { SeoService } from '../../core/seo/seo.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  constructor(seo: SeoService) { 
    seo.metaTags('homepage titulo','homepage muito longa descriçao','https://loremflickr.com/320/240');
  }

  ngOnInit() {
  }

}
