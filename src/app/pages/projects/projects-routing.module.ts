import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsComponent } from './projects.component';

const projectsRoutes: Routes = [
  { path: '', component: ProjectsComponent, data: { title: 'main.menu.projects' } },
];

@NgModule({
  imports: [
    RouterModule.forChild(projectsRoutes),
  ],
  exports: [RouterModule]
})
export class ProjectsRoutingModule { }
