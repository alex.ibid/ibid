import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { TranslateModule } from '@ngx-translate/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LayoutModule } from '@angular/cdk/layout';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialsModule } from './materials/materials.module';
import { FontawesomeModule } from './fontawesome/fontawesome.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    
    TranslateModule,

    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    FlexLayoutModule,
    MaterialsModule,

    FontawesomeModule

  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    TranslateModule,
    
    FlexLayoutModule,
    MaterialsModule,
    
    FontawesomeModule
  ]
})
export class SharedModule { }
