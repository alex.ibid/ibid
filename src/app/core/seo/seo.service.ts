import { Title, Meta } from '@angular/platform-browser';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { environment } from '../../../environments/environment';
import { filter } from 'rxjs/internal/operators/filter';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable()
export class SeoService {

  public header: BehaviorSubject<string>;
  
  constructor(
    private translateService: TranslateService,
    private title: Title,
    private meta: Meta
  ) {
    this.header = new BehaviorSubject<string>(title.getTitle());
  }

  setSeo(
    snapshot: ActivatedRouteSnapshot,
    lazyTranslateService?: TranslateService
  ) {
    let lastChild = snapshot;
    while (lastChild.children.length) {
      lastChild = lastChild.children[0];
    }
    const { title } = lastChild.data;
    const translate = lazyTranslateService || this.translateService;
    if (title) {
      translate
        .get(title)
        .pipe(filter(translatedTitle => translatedTitle !== title))
        .subscribe(translatedTitle => {
          this.title.setTitle(`${translatedTitle} - ${environment.appName}`);
          this.header.next(`${translatedTitle}`);
        }
          
        );
    } else {
      this.title.setTitle(environment.appName);
    }
  }

  metaTags(title, description, img) {
    this.meta.updateTag({ name: 'twitter:card', content: 'summary' });
    this.meta.updateTag({ name: 'twitter:site', content: '@ibid' });
    this.meta.updateTag({ name: 'twitter:title', content: title });
    this.meta.updateTag({ name: 'twitter:description', content: description });
    this.meta.updateTag({ name: 'twitter:image', content: img });
  }

}
