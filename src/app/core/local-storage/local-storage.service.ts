import { Injectable, Inject, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

const APP_PREFIX = 'IBID-';

@Injectable()
export class LocalStorageService {
  static platformId: any;
  constructor(@Inject(PLATFORM_ID) private platformId: Object) {}

  static loadInitialState() {
    return Object.keys(localStorage).reduce((state: any, storageKey) => {
      if (storageKey.includes(APP_PREFIX)) {
        const stateKeys = storageKey
          .replace(APP_PREFIX, '')
          .toLowerCase()
          .split('.')
          .map(key =>
            key
              .split('-')
              .map(
                (token, index) =>
                  index === 0
                    ? token
                    : token.charAt(0).toUpperCase() + token.slice(1)
              )
              .join('')
          );
        let currentStateRef = state;
        stateKeys.forEach((key, index) => {
          if (index === stateKeys.length - 1) {
            if ( key === 'locale' ) { 
              currentStateRef[key] = localStorage.getItem(storageKey);
            } else {
              currentStateRef[key] = JSON.parse(localStorage.getItem(storageKey));
            }
            return;
          }
          currentStateRef[key] = currentStateRef[key] || {};
          currentStateRef = currentStateRef[key];
        });
      }
      return state;
    }, {});
  }

  setItem(key: string, value: any) {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.setItem(`${APP_PREFIX}${key}`, JSON.stringify(value));
    }
  }

  getItem(key: string) {
    if (isPlatformBrowser(this.platformId)) {
      return JSON.parse(localStorage.getItem(`${APP_PREFIX}${key}`));
    }
  }

  getValue(key: string) {
    if (isPlatformBrowser(this.platformId)) {
      return localStorage.getItem(`${APP_PREFIX}${key}`);
    }
  }

  removeItem(key: string) {
    if (isPlatformBrowser(this.platformId)) {
      localStorage.removeItem(`${APP_PREFIX}${key}`);
    }
  }

}
