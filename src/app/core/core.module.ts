import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './core.state';
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';
import { environment } from '../../environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { SeoService } from './seo/seo.service';
import { LocalStorageService } from './local-storage/local-storage.service';
import { EffectsModule } from '@ngrx/effects';
import { CustomSerializer } from './router/custom-serializer';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forRoot([]),

    // environment.production ? [] : StoreDevtoolsModule.instrument({name: 'Ibid.'})
    StoreDevtoolsModule.instrument({name: 'Ibid.'})
  ],
  exports: [],
  providers:[
    SeoService,
    LocalStorageService,
    { provide: RouterStateSerializer, useClass: CustomSerializer }
  ]
})
export class CoreModule { }


