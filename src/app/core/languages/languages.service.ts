import { Injectable } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { Inject, PLATFORM_ID, Optional } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LanguagesService {

  constructor(
    private translate: TranslateService,
    @Optional() @Inject(REQUEST) private request: Request, 
    @Inject(PLATFORM_ID) private platformId: any
  ) { }

  public getLang(): string {
    let language: string;
    if (isPlatformBrowser(this.platformId)) {
      language = this.translate.getBrowserLang();
    } else {
      language = (this.request.headers['accept-language'] || '').substring(0, 2);
    }
    return language;
  }
}
