import { ActionReducerMap, createFeatureSelector, MetaReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';

import { stateLocalStorage } from './local-storage/state-local-storage.reducer';
import { debug } from '../core/debug/debug.reducer';

import { RouterStateUrl } from '../core/router/router.state';
import { environment } from '../../environments/environment';

export const reducers: ActionReducerMap<AppState> = {
    router: routerReducer
};

export const selectRouterState = createFeatureSelector<AppState, RouterReducerState<RouterStateUrl>>('router');

export const metaReducers: MetaReducer<AppState>[] = [
    stateLocalStorage
  ];
  if (!environment.production) {
    metaReducers.unshift(storeFreeze);
    if (!environment.test) {
      metaReducers.unshift(debug);
    }
  }

export interface AppState {
    router: RouterReducerState<RouterStateUrl>;
}