import { Action } from '@ngrx/store';

import { LanguageType } from '../languages';

export enum SettingsActionTypes {
  CHANGE_LANGUAGE = 'Settings action: Change Language',
  CHANGE_THEME = 'Settings action: Change Theme'
}

export class ActionSettingsChangeLanguage implements Action {
  readonly type = SettingsActionTypes.CHANGE_LANGUAGE;
  constructor(readonly payload: { language: LanguageType }) { }
}

export class ActionSettingsChangeTheme implements Action {
  readonly type = SettingsActionTypes.CHANGE_THEME;
  constructor(readonly payload: { theme: string }) { }
}


export type SettingsActions =
  | ActionSettingsChangeLanguage
  | ActionSettingsChangeTheme;
