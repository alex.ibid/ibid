import { ActivationEnd, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { TranslateService } from '@ngx-translate/core';

import { SettingsActionTypes, SettingsActions } from './settings.actions';
import { selectSettingsState, selectSettingsTheme } from './settings.selectors';

import { State } from './settings.model';

import { LocalStorageService } from '../core/local-storage/local-storage.service';
import { SeoService } from '../core/seo/seo.service';
import { OverlayContainer } from '@angular/cdk/overlay';
import { of, merge } from 'rxjs';
import { withLatestFrom, tap, map, distinctUntilChanged, filter } from 'rxjs/operators';

export const SETTINGS_KEY = 'SETTINGS';
export const LOCALE_KEY = 'LOCALE';

const INIT = of('ibid-init-effect-trigger');

@Injectable()
export class SettingsEffects {
  constructor(
    private actions$: Actions<SettingsActions>,
    private store: Store<State>,
    private router: Router,
    private localStorageService: LocalStorageService,
    private translateService: TranslateService,
    private seoService: SeoService,
    private overlayContainer: OverlayContainer,
  ) {}


  @Effect({ dispatch: false })
  persistSettings = this.actions$.pipe(
    ofType(
      SettingsActionTypes.CHANGE_LANGUAGE,
      SettingsActionTypes.CHANGE_THEME
    ),
    withLatestFrom(this.store.pipe(select(selectSettingsState))),
    tap(([action, settings]) =>
      {
        this.localStorageService.setItem(LOCALE_KEY, settings.language)
        this.localStorageService.setItem(SETTINGS_KEY, settings)
      }
    )
  );

  @Effect({ dispatch: false })
  setTranslateServiceLanguage = this.store.pipe(
      select(selectSettingsState),
      map(settings => settings.language),
      distinctUntilChanged(),
      tap(language => {
        this.translateService.use(language);
      })
  );

  @Effect({ dispatch: false })
  setTitle = merge(
      this.actions$.pipe(ofType(SettingsActionTypes.CHANGE_LANGUAGE)),
      this.router.events.pipe(filter(event => event instanceof ActivationEnd))
    ).pipe(
      tap(() => {
        this.seoService.setSeo(
          this.router.routerState.snapshot.root,
          this.translateService
        );
      })
  );

  @Effect({ dispatch: false })
  selectTheme = merge(
    INIT,
    this.actions$.pipe(ofType(SettingsActionTypes.CHANGE_THEME))
  ).pipe(
    withLatestFrom(this.store.pipe(select(selectSettingsTheme))),
    tap(([action, selectedTheme]) => {
      const classList = this.overlayContainer.getContainerElement().classList;
      const removeTheme = Array.from(classList).filter((theme: string) =>
        theme.includes('-theme')
      );
      if (removeTheme.length) {
        classList.remove(...removeTheme);
      }
      classList.add(selectedTheme);
    })
  );
}
