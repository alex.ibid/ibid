import { AppState } from '../core/core.state';

export const NIGHT_THEME = 'night-theme';
export const DAY_THEME = 'day-theme';

export interface SettingsState {
  language: string;
  theme: string;
}

export interface State extends AppState {
  settings: SettingsState;
}
