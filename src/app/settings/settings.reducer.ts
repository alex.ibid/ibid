import { SettingsState } from './settings.model';
import { SettingsActions, SettingsActionTypes } from './settings.actions';

export const initialState: SettingsState = {
  language: 'en',
  theme: 'day-theme'
};

export function settingsReducer(
  state: SettingsState = initialState,
  action: SettingsActions
): SettingsState {
  switch (action.type) {
    case SettingsActionTypes.CHANGE_LANGUAGE:
      return { ...state, ...action.payload };
    case SettingsActionTypes.CHANGE_THEME:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
