import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as fs from 'fs';


admin.initializeApp(functions.config().firebase);

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

export const universal = functions.https.onRequest((request, response) => {
    require(`${process.cwd()}/dist/ibid-webpack/server`).app(request, response);
});

let collectionName: string = 'routes';
let subCollection = process.argv[3];

let db = admin.firestore();
db.settings({ timestampsInSnapshots: true });

let data: any = {};
data[collectionName] = {};

let results = db.collection(collectionName)
.get()
.then(snapshot => {
  snapshot.forEach(doc => {
    data[collectionName][doc.id] = doc.data();
  })
  return data;
})
.catch(error => {
  console.log(error);
})

results.then(dt => {  
  getSubCollection(dt).then(() => {   
    fs.writeFile("../public/assets/i18n/" + collectionName + ".json", JSON.stringify(data), function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
  })  
})

async function getSubCollection(dt: any){
  for (let [key, value] of (<any>Object).entries([dt[collectionName]][0])){
    console.log([key, value]);
    if(subCollection !== undefined){
      data[collectionName][key]['subCollection'] = {};
      await addSubCollection(key, data[collectionName][key]['subCollection']);            
    }          
  }  
}

function addSubCollection(key: string, subData: any){
  return new Promise(resolve => {
    db.collection(collectionName).doc(key).collection(subCollection).get()
    .then(snapshot => {      
      snapshot.forEach(subDoc => {             
        subData[subDoc.id] =  subDoc.data();
        resolve('Added data');                                                                 
      })
    })
  })
}

